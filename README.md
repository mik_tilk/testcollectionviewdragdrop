# README #

This Module implements Drag&Drop in collection view. It support dragging for cells. It can't support dragging for sections. It can't provide saving of position change for collectionDataSource models. User should support it by himself implementing _MMMDragAndDropDelegateProtocol_.

### What is this repository for? ###

* __Module that implements Drag&Drop in collection view.__
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Installation
    + Copy files from dir /MMMDragDropModule/ into your project
    + Import MMMDragCellController into owner of collection view
    + Initiate MMMDragCellController object with collection view and delegate (usually collectionView data source) 
    + Implement MMMDragAndDropDelegateProtocol in delegate
* Configuration
    + MMMDragDropConstants contains all constants for module behavior tweaking by user.
* Dependencies
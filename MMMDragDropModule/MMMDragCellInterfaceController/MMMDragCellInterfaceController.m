//
//  MMMDragCellInterfaceController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/26/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMDragCellInterfaceController.h"

@interface MMMDragCellInterfaceController ()

@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) UIView *superView;
@property(nonatomic, strong) UIView *draggedCellSnapshot;
@property(nonatomic, strong) UICollectionViewCell *draggedCell;
@property(nonatomic, assign) CGPoint delta;

@end

@implementation MMMDragCellInterfaceController

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
{
    self = [super init];
    
    if (self)
    {
        _collectionView = collectionView;
        _draggedCellSnapshot = nil;
        [_draggedCellSnapshot setHidden:YES];
    }
    
    return self;
}

#pragma mark - .h

/*!
 @brief Method does animation on start dragging
 */
- (void)initiateDraggingByIndexPath:(NSIndexPath *)indexPath touchPointInCollection:(CGPoint)touchPointInCollection
{
    _superView = [_collectionView superview];
    CGPoint touchPointInPage = [_superView convertPoint:touchPointInCollection fromView:_collectionView];
    
    _draggedCell = [_collectionView cellForItemAtIndexPath:indexPath];
    
    _delta = CGPointMake([_draggedCell center].x - touchPointInCollection.x, [_draggedCell center].y - touchPointInCollection.y);
    
    _draggedCellSnapshot = [_draggedCell snapshotViewAfterScreenUpdates:YES];
    [_draggedCellSnapshot setHidden:NO];
    [_draggedCellSnapshot setCenter:SumCGPoints(touchPointInPage, _delta)];
    
    [UIView animateWithDuration:.25 animations:^
    {
        _draggedCellSnapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
        _draggedCellSnapshot.alpha = .9;
        _draggedCell.alpha = .0;
    }
                    completion:^(BOOL finished)
                    {
                        [_draggedCell setHidden:YES];
                    }];
    
    [_superView addSubview:_draggedCellSnapshot];
}

/*!
 @brief Method updates snapshot position and visibility while dragging
 */
- (void)updateViewsByTouchPointInPage:(CGPoint)touchPointInCollection
{
    CGPoint touchPointInPage = [_superView convertPoint:touchPointInCollection fromView:_collectionView];
    [_draggedCellSnapshot setCenter:SumCGPoints(touchPointInPage, _delta)];
    [self updateSnapshotStateByGesturePoint:touchPointInPage];
}

/*!
 @brief Method updates snapshot and dragged cell positions and visibilities on finish dragging
 */
- (void)finishDraggingCell
{
    [UIView animateWithDuration:.25 animations:^
    {
        [_draggedCellSnapshot setCenter:[_superView convertPoint:[_draggedCell center] fromView:_collectionView]];
        _draggedCellSnapshot.transform = CGAffineTransformMakeScale(1., 1.);
        _draggedCell.alpha = 1.;
    }
    completion:^(BOOL finished)
    {
        [_draggedCellSnapshot setHidden:YES];
        [_draggedCell setHidden:NO];
    }];
}

#pragma mark - private

/*!
 @brief Method updates snapshot visibility while dragging
 */
- (void)updateSnapshotStateByGesturePoint:(CGPoint)touchPointInPage
{
    CGPoint touchPointInCollection = [_collectionView convertPoint:touchPointInPage fromView:_superView];
    BOOL isInsideCollectionView = CGRectContainsPoint([_collectionView bounds], touchPointInCollection);

    if (isInsideCollectionView)
    {
        _draggedCellSnapshot.alpha = .9;
    }
    else
    {
        _draggedCellSnapshot.alpha = .2;
    }
}

CGPoint SumCGPoints(CGPoint p1, CGPoint p2)
{
    return (CGPoint){p1.x + p2.x, p1.y + p2.y };
}

@end

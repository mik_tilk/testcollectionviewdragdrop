//
//  MMMDragCellController.m
//
//
//  Created by Kholomeev on 11/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMMDragCellController.h"
#import "MMMDragDropConstants.h"
#import "MMMDragCellInterfaceController.h"

#define LESS_THAN_iOS7  ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] == NSOrderedAscending)
#define iOS7            ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)
#define iOS7_OR_GREATER ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)
#define iOS8_OR_GREATER ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending)
#define LESS_THAN_iOS6  ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
#define LESS_THAN_iOS9  ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] == NSOrderedAscending)
#define iOS9            ([[[UIDevice currentDevice] systemVersion] compare:@"9.0" options:NSNumericSearch] != NSOrderedAscending)

/*!
 @brief Enum that wraps scroll states
*/
typedef NS_ENUM(NSUInteger, MMMScrollState)
{
    PointAtNormalArea,
    PointAtBottomBorderSlowArea,
    PointAtTopBorderSlowArea,
    PointAtBottomBorderFastArea,
    PointAtTopBorderFastArea,
    PointAtLeftBorderSlowArea,
    PointAtRightBorderSlowArea,
    PointAtLeftBorderFastArea,
    PointAtRightBorderFastArea,
};

@interface MMMDragCellController ()

    /// A collection view
@property(nonatomic, strong) UICollectionView *collectionView;
    /// A collection view datasource
@property(nonatomic, weak) id<MMMDragAndDropDelegateProtocol> delegate;
    /// Class that implements dragging interface visual behaviour.
@property(nonatomic, strong) MMMDragCellInterfaceController *dragCellInterface;

    /// This is a view that will be contain a snapshot view
@property(nonatomic, strong) UIView *superView;
    /// Class that contains config constants
@property(nonatomic, strong) MMMDragDropConstants *config;
    /// Dragged cell view
@property(nonatomic, strong) UICollectionViewCell *draggedCell;
    /// A flag that shows dragging state: dragging ON or OFF
@property(nonatomic, assign, getter = isDragging) BOOL dragging;
    /// TCurrent scrolling state
@property(nonatomic, assign) MMMScrollState scrollState;
    /// A flag, that shows current scrolling direction
@property(nonatomic, assign) BOOL scrollDirectionVertical;
    /// Timer that responsible for continuous content view sliding, imitating scrolling
@property(nonatomic, strong) CADisplayLink *scrollUpdateTimer;
    /// An index path of dragged cell
@property(nonatomic, strong) NSIndexPath *draggedIndexPath;
    /// An index path were dragged cell should be placed
@property(nonatomic, strong) NSIndexPath *destinationIndexPath;
    /// Realtime coordinate were current gesture take place in collectionView coordinate system
@property(nonatomic, assign) CGPoint touchPointInCollection;

@end

@implementation MMMDragCellController

static NSInteger scrollSpeed = 0;

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView delegate:(id<MMMDragAndDropDelegateProtocol>)delegate;
{
    self = [super init];
    
    if (self != nil)
    {
        _delegate = delegate;
        _collectionView = collectionView;
        _superView = [_collectionView superview];

        _dragCellInterface = [[MMMDragCellInterfaceController alloc] initWithCollectionView:_collectionView];
        [_dragCellInterface setDelegate:self];
        
        _config = [MMMDragDropConstants new];
        
        _dragging = NO;
        _scrollState = PointAtNormalArea;
        _scrollUpdateTimer = nil;
        _draggedIndexPath = nil;
        _destinationIndexPath = nil;
        
        UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanOnPage:)];
        [longPressGesture setDelegate:self];
        [_collectionView addGestureRecognizer:longPressGesture];
    }
    
    return self;
}

#pragma mark - GestureDelegate

/*!
 @brief Method handles LongPress gesture
 */
- (void)handlePanOnPage:(UILongPressGestureRecognizer *)gesture
{
    _touchPointInCollection = [gesture locationInView:_collectionView];
    
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:_touchPointInCollection];
    
    switch ([gesture state])
    {
        case UIGestureRecognizerStateBegan:
        {
                /// In case there is no cell under gesture - no dragging
            if (indexPath == nil)
            {
                return;
            }
            
            _scrollDirectionVertical = [_collectionView contentSize].width > [_collectionView contentSize].height ? 0 : 1;
            [self setDragging:YES];
            [self setInitialIndexPathsOnDrag:indexPath];

            if ([_delegate respondsToSelector:@selector(dragController:willStartDraggingIndexPath:)])
            {
                [_delegate dragController:self willStartDraggingIndexPath:indexPath];
            }
            
            [_dragCellInterface initiateDraggingByIndexPath:indexPath touchPointInCollection:_touchPointInCollection];
        }
        break;
        case UIGestureRecognizerStateChanged:
        {
            if ([self isDragging])
            {
                [_dragCellInterface updateViewsByTouchPointInPage:_touchPointInCollection];
                [self dragDraggedCellToTouchPointInCollection];
            }
        }
        break;
        default:
        {
            if ([self isDragging])
            {
                _dragging = NO;
                [self stopScrollUpdateTimer];
                
                [_dragCellInterface finishDraggingCell];
                
                if ([_delegate respondsToSelector:@selector(dragControllerWillFinishDragging:)])
                {
                    [_delegate dragControllerWillFinishDragging:self];
                }
            }
        }
    }
}

#pragma mark - private

/*!
 @brief Method wraps all actions for dragging cell
 */
- (void)dragDraggedCellToTouchPointInCollection
{
    [self calculatesDestinationIndexPath];
    
    if (_scrollDirectionVertical)
    {
        [self doVerticalDrag];
    }
    else
    {
        [self doHorizontalDrag];
    }
}

/*!
 @brief Method inits draggedIndexPath and destinationIndexPath
 */
- (void)setInitialIndexPathsOnDrag:(NSIndexPath *)draggedIndex
{
    _draggedIndexPath = draggedIndex;
    [self calculatesDestinationIndexPath];
}

/*!
 @brief Method does vertical drag
 */
- (void) doVerticalDrag
{
    MMMScrollState currentScrollState = [self getScrollingStateForGestureTouchPointInCollection];
    
    if (_scrollUpdateTimer == nil || _scrollState != currentScrollState)
    {
        _scrollState = currentScrollState;
        [self updateScrollTimerSelectop:@selector(scrollVertical)];
        
        switch (_scrollState)
        {
        case PointAtBottomBorderSlowArea:
            scrollSpeed = [_config scrollSpeedSlow];
            break;
        case PointAtBottomBorderFastArea:
            scrollSpeed = [_config scrollSpeedFast];
            break;
        case PointAtTopBorderSlowArea:
            scrollSpeed = -[_config scrollSpeedSlow];
            break;
        case PointAtTopBorderFastArea:
            scrollSpeed = -[_config scrollSpeedFast];
            break;
        default:
            scrollSpeed = 0;
            [self stopScrollUpdateTimer];
            [self moveCellFromIndex:_draggedIndexPath toIndex:_destinationIndexPath];
            break;
        }
    }
}

/*!
 @brief Method does horizontal drag
 */
- (void) doHorizontalDrag
{
    MMMScrollState currentScrollState = [self getScrollingStateForGestureTouchPointInCollection];
    
    if (_scrollUpdateTimer == nil || _scrollState != currentScrollState)
    {
        _scrollState = currentScrollState;
        [self updateScrollTimerSelectop:@selector(scrollHorizontal)];
        
        switch (_scrollState)
        {
            case PointAtRightBorderSlowArea:
                scrollSpeed = [_config scrollSpeedSlow];
                break;
            case PointAtRightBorderFastArea:
                scrollSpeed = [_config scrollSpeedFast];
                break;
            case PointAtLeftBorderSlowArea:
                scrollSpeed = -[_config scrollSpeedSlow];
                break;
            case PointAtLeftBorderFastArea:
                scrollSpeed = -[_config scrollSpeedFast];
                break;
            default:
                scrollSpeed = 0;
                [self stopScrollUpdateTimer];
                [self moveCellFromIndex:_draggedIndexPath toIndex:_destinationIndexPath];
                break;
        }
    }
}

/*!
 @brief Method updates scrolling timer for given selector
*/
- (void)updateScrollTimerSelectop:(SEL)selector
{
    [self stopScrollUpdateTimer];
    _scrollUpdateTimer = [CADisplayLink displayLinkWithTarget:self selector:selector];
    [_scrollUpdateTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

/*!
 @brief Method returns scrolling state
 */
- (MMMScrollState)getScrollingStateForGestureTouchPointInCollection
{
    CGPoint offset = [_collectionView contentOffset];
    CGSize size = [_collectionView bounds].size;
    
    if (_touchPointInCollection.y > (offset.y + size.height - [_config scrollFastArea]))
    {
        return PointAtBottomBorderFastArea;
    }
    else if (_touchPointInCollection.y > (offset.y + size.height - [_config scrollSlowArea]))
    {
        return PointAtBottomBorderSlowArea;
    }
    else if (_touchPointInCollection.y < (offset.y + [_config scrollFastArea]))
    {
        return PointAtTopBorderFastArea;
    }
    else if (_touchPointInCollection.y < (offset.y + [_config scrollSlowArea]))
    {
        return PointAtTopBorderSlowArea;
    }
    else if (_touchPointInCollection.x > (offset.x + size.width - [_config scrollFastArea]))
    {
        return PointAtRightBorderFastArea;
    }
    else if (_touchPointInCollection.x > (offset.x + size.width - [_config scrollSlowArea]))
    {
        return PointAtRightBorderSlowArea;
    }
    else if (_touchPointInCollection.x < (offset.x + [_config scrollFastArea]))
    {
        return PointAtLeftBorderFastArea;
    }
    else if (_touchPointInCollection.x < (offset.x + [_config scrollSlowArea]))
    {
        return PointAtLeftBorderSlowArea;
    }
    
    return PointAtNormalArea;
}

/*!
 @brief Method imitates vertical scrolling
 
 @discussion This method slides content vertical on scrollSpeed value. Also it changes touchPointInCOllection. Used as selector in timer
 */
- (void)scrollVertical
{
    CGPoint newOffset = CGPointMake([[self collectionView] contentOffset].x, [[self collectionView] contentOffset].y + scrollSpeed);
    
    if (newOffset.y + [[self collectionView] bounds].size.height < [[self collectionView] contentSize].height && newOffset.y >= 0)
    {
        [[self collectionView] setContentOffset:newOffset];
        _touchPointInCollection.y += scrollSpeed;
    }
    else
    {
        [self stopScrollUpdateTimer];
    }
    
    [self updateDraggedCellWhileScrolling];
}

/*!
 @brief Method imitates horisontal scrolling
 
 @discussion This method slides content horisontaly on scrollSpeed value. Also it changes touchPointInCOllection. Used as selector in timer
 */
- (void)scrollHorizontal
{
    CGPoint newOffset = CGPointMake([[self collectionView] contentOffset].x + scrollSpeed, [[self collectionView] contentOffset].y);
    
    if (newOffset.x + [[self collectionView] bounds].size.width < [[self collectionView] contentSize].width && newOffset.x >= 0)
    {
        [[self collectionView] setContentOffset:newOffset];
        _touchPointInCollection.x += scrollSpeed;
    }
    else
    {
        [self stopScrollUpdateTimer];
    }
    
    [self updateDraggedCellWhileScrolling];
}

/*!
 @brief Method that updates draggedCellIndexPath property while scrolling usind data from touchPointInCollection property
*/
- (void)updateDraggedCellWhileScrolling
{
    [self calculatesDestinationIndexPath];
    
    if (![_draggedIndexPath isEqual:_destinationIndexPath])
    {
        [self moveCellFromIndex:_draggedIndexPath toIndex:_destinationIndexPath];
    }
}

/*!
 @brief Method inserts dragged cell in to new index path
 
 @discussion Method compares index path of cell that will be moved and index path of cell where the first will be moved, and if they are not equal it sends to delegate both index paths. After that it commands collection view to insert first cell in to second index path.
 */
- (void)moveCellFromIndex:(NSIndexPath *)indexFrom toIndex:(NSIndexPath *)indexTo
{
    if (![indexFrom isEqual:indexTo])
    {
        [_delegate dragController:self moveItemAtIndexPath:indexFrom toIndexPath:indexTo];
        
        [[self collectionView] moveItemAtIndexPath:indexFrom toIndexPath:indexTo];
          _draggedIndexPath = indexTo;
    }
}

/*!
 @brief Method calculates destination's index path by touchPointInCollection property
 
 @discussion Method sets destination's index path according to index path of the cell under gesture. If there is no cell under gesture then destination's indexpath calculates according current scrollState.
 */
- (void)calculatesDestinationIndexPath
{
    NSIndexPath *indexPath = [[self collectionView] indexPathForItemAtPoint:_touchPointInCollection];

    if (indexPath == nil)
    {
        if (_scrollDirectionVertical)
        {
            switch (_scrollState)
            {
                case PointAtBottomBorderFastArea:
                case PointAtBottomBorderSlowArea:
                    indexPath = [self getMaxVisibleIndexPath];
                    break;
                case PointAtTopBorderFastArea:
                case PointAtTopBorderSlowArea:
                    indexPath = [self getMinVisibleIndexPath];
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (_scrollState)
            {
                case PointAtRightBorderFastArea:
                case PointAtRightBorderSlowArea:
                    indexPath = [self getMaxVisibleIndexPath];
                    break;
                case PointAtLeftBorderFastArea:
                case PointAtLeftBorderSlowArea:
                    indexPath = [self getMinVisibleIndexPath];
                    break;
                default:
                    break;
            }
        }
    }
    
    if (indexPath != nil && ![indexPath isEqual:_destinationIndexPath])
    {
        _destinationIndexPath = indexPath;
    }
}

/*!
 @brief Method returns min index path of visible cells
 
 @return NSIndexPath
 */
- (NSIndexPath *)getMinVisibleIndexPath
{
    NSArray *visibleCells = [[self collectionView] visibleCells];
    NSIndexPath *resultIndex = [NSIndexPath indexPathWithIndex:NSIntegerMax];
    NSIndexPath *currentIndex = nil;
    
    for (UICollectionViewCell *cell in visibleCells)
    {
        currentIndex = [[self collectionView] indexPathForCell:cell];
        
        if ([resultIndex compare:currentIndex] == NSOrderedDescending)
        {
            resultIndex = currentIndex;
        }
    }
    
    return resultIndex;
}

/*!
 @brief Method returns max index path of visible cells
 
 @return NSIndexPath
 */
- (NSIndexPath *)getMaxVisibleIndexPath
{
    NSArray *visibleCells = [[self collectionView] visibleCells];
    NSIndexPath *resultIndex = [NSIndexPath indexPathWithIndex:0];
    NSIndexPath *currentIndex = nil;
    
    for (UICollectionViewCell *cell in visibleCells)
    {
        currentIndex = [[self collectionView] indexPathForCell:cell];
        
        if ([resultIndex compare:currentIndex] == NSOrderedAscending)
        {
            resultIndex = currentIndex;
        }
    }
    
    return resultIndex;
}

- (void)stopScrollUpdateTimer
{
    if (_scrollUpdateTimer != nil)
    {
        [_scrollUpdateTimer invalidate];
        _scrollUpdateTimer = nil;
    }
}

@end

//
//  MMMDragDropConstants.m
//  TestCollectionViewDragDrop
//
//  Created by Kholomeev on 12/5/15.
//  Copyright © 2015 Kholomeev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMDragDropConstants.h"

@implementation MMMDragDropConstants

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _scrollSpeedSlow = 5;
        _scrollSpeedFast = 20;
        _scrollSlowArea = 80;
        _scrollFastArea = 40;
        
        _animationDuration = .25;
        _scaleZoomIn = 1.05;
        _scaleZoomNo = 1;
        _alphaSnapshotCorrectLocation = .9;
        _alphaSnapshotIncorrectLocation = .2;
        _alphaHiddenCell = .0;
        _alphaRevealedCell = 1;
    }
    
    return self;
}

@end
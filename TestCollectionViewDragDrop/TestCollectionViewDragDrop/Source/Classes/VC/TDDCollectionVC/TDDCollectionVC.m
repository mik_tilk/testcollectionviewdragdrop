//
//  ViewController.m
//  TestCollectionViewDragDrop
//
//  Created by Kholomeev on 12/1/15.
//  Copyright © 2015 Kholomeev. All rights reserved.
//

#import "TDDCollectionVC.h"
#import "TDDCollectionDS.h"
#import "MMMDragCellController.h"

@interface TDDCollectionVC ()

@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(weak, nonatomic) IBOutlet TDDCollectionDS *collectionDS;

@property(nonatomic, strong) MMMDragCellController *dragController;

@end

@implementation TDDCollectionVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _dragController = [[MMMDragCellController alloc] initWithCollectionView:_collectionView delegate:_collectionDS];
}

@end

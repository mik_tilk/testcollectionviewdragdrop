//
//  MMMDragCellController.h
//  
//
//  Created by Kholomeev on 11/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//
//  Main class wich implements Drag&Drop.
//
//    - Delegate - usualy collectionView's DataSource
//    - MMMDragCellController assigns LongTapGesture to collectionView
//    - MMMDragCellInterfaceController is a class that responsible for visual behavior while Dragging
//    - According to MMMDragAndDropDelegateProtocol, MMMDragCellController sends notifications
//      to delegate: on draging start, on update position, on dragging end.
//    - Scroll direction got from [collecitonView contentSize] analize.
//  

#import <Foundation/Foundation.h>
#import "MMMDragAndDropDelegateProtocol.h"

@class UICollectionView;

@interface MMMDragCellController : NSObject<UIGestureRecognizerDelegate>

/*!
 @brief It initiate DragCellController with collectionView and delegate, usualy collectionView's DataSource.
 
 @discussion This method accepts collectionView and delegate (usualy collectionView's DataSource) and initiate DragCellController.
 It assigns LongTapGesture to collectionView and respond to it. 
 On initiation, DragCellController, also initiate DragCellInterfaceController that responsible for visual effects while Dragging.
 Delegate must conform MMMDragAndDropDelegateProtocol for being able receive drag&drop notifications and datas updates.
 Module get scroll direction from analize of @c[collecitonView contentSize] width and height.
 
 @param  collectionView - The target collectionView that needs draqgging ability.
 @param  delegate - An object that receives notifications and datas about dragging events.
 
 @return MMMDragCellController object.
 */
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView delegate:(id<MMMDragAndDropDelegateProtocol>)delegate;

@end

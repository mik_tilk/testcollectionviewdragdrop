//
//  TDDCollectionDS.h
//  TestCollectionViewDragDrop
//
//  Created by Kholomeev on 12/1/15.
//  Copyright © 2015 Kholomeev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MMMDragAndDropDelegateProtocol.h"

@interface TDDCollectionDS : NSObject<MMMDragAndDropDelegateProtocol>

@end

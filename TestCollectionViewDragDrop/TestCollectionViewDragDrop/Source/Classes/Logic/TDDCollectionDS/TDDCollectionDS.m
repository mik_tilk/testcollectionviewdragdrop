//
//  TDDCollectionDS.m
//  TestCollectionViewDragDrop
//
//  Created by Kholomeev on 12/1/15.
//  Copyright © 2015 Kholomeev. All rights reserved.
//

#import "TDDCollectionDS.h"

@interface TDDCollectionDS ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, copy) NSArray *modelsArray;

@end

@implementation TDDCollectionDS

const NSString *cellID = @"cell";

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        NSMutableArray *mutArray = [NSMutableArray array];
        
        for (NSInteger i = 0; i < 255; i++)
        {
            [mutArray addObject:@(i)];
        }
        
        _modelsArray = (NSArray *)mutArray;
    }
    
    return self;
}

#pragma mark - DragAndDropProtocol

- (void)dragController:(id)dragController willStartDraggingIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)dragController:(id)dragController moveItemAtIndexPath:(NSIndexPath *)indexFrom toIndexPath:(NSIndexPath *)indexTo
{
    if (![indexFrom isEqual:indexTo])
    {
        NSInteger from = [indexFrom item];
        NSInteger into = [indexTo item];
        
        NSMutableArray *mutArray = [NSMutableArray arrayWithArray:[self modelsArray]];
        NSString *draggedTrack = [mutArray objectAtIndex:from];
        [mutArray removeObject:draggedTrack];
        [mutArray insertObject:draggedTrack atIndex:into];
        
        [self setModelsArray:(NSArray *)mutArray];
    }
}

#pragma mark - UICollectionViewProtocol

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_modelsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    CGFloat whiteStage = (float)[[_modelsArray objectAtIndex:[indexPath item]] integerValue]/256;
    
    [cell setBackgroundColor:[UIColor colorWithWhite:whiteStage alpha:1.]];
    
    return cell;
}

#pragma mark - UICollectionDataSourceProtocol

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath
{
    
}


@end

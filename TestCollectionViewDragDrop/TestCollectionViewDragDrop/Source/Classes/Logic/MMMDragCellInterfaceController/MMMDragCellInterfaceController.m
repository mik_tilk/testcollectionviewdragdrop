//
//  MMMDragCellInterfaceController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/26/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMDragCellInterfaceController.h"
#import "MMMDragDropConstants.h"

@interface MMMDragCellInterfaceController ()

    /// A collection view
@property(nonatomic, strong) UICollectionView *collectionView;
    /// This is a view that will be contain a snapshot view
@property(nonatomic, strong) UIView *superView;
    /// This is a view with dragged cell snapshot
@property(nonatomic, strong) UIView *draggedCellSnapshot;
    /// Dragged cell
@property(nonatomic, strong) UICollectionViewCell *draggedCell;
    /// Class that contains config constants
@property(nonatomic, strong) MMMDragDropConstants *config;
    /// This is a delta between center of snapshot and point of touch
@property(nonatomic, assign) CGPoint delta;

@end

@implementation MMMDragCellInterfaceController

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
{
    self = [super init];
    
    if (self)
    {
        _collectionView = collectionView;
        _draggedCellSnapshot = nil;
        [_draggedCellSnapshot setHidden:YES];
        _config = [MMMDragDropConstants new];
    }
    
    return self;
}

#pragma mark - .h

/*!
 @brief Method does animation on start dragging
 */
- (void)initiateDraggingByIndexPath:(NSIndexPath *)indexPath touchPointInCollection:(CGPoint)touchPointInCollection
{
    _superView = [_collectionView superview];
    CGPoint touchPointInPage = [_superView convertPoint:touchPointInCollection fromView:_collectionView];
    
    _draggedCell = [_collectionView cellForItemAtIndexPath:indexPath];
    
    _delta = CGPointMake([_draggedCell center].x - touchPointInCollection.x, [_draggedCell center].y - touchPointInCollection.y);
    
    _draggedCellSnapshot = [_draggedCell snapshotViewAfterScreenUpdates:YES];
    [_draggedCellSnapshot setHidden:NO];
    [_draggedCellSnapshot setCenter:SumCGPoints(touchPointInPage, _delta)];
    
    [UIView animateWithDuration:[_config animationDuration] animations:^
    {
        _draggedCellSnapshot.transform = CGAffineTransformMakeScale([_config scaleZoomIn], [_config scaleZoomIn]);
        _draggedCellSnapshot.alpha = [_config alphaSnapshotCorrectLocation];
        _draggedCell.alpha = [_config alphaHiddenCell];
    }
                    completion:^(BOOL finished)
                    {
                        [_draggedCell setHidden:YES];
                    }];
    
    [_superView addSubview:_draggedCellSnapshot];
}

/*!
 @brief Method updates snapshot position and visibility while dragging
 */
- (void)updateViewsByTouchPointInPage:(CGPoint)touchPointInCollection
{
    CGPoint touchPointInPage = [_superView convertPoint:touchPointInCollection fromView:_collectionView];
    [_draggedCellSnapshot setCenter:SumCGPoints(touchPointInPage, _delta)];
    [self updateSnapshotStateByGesturePoint:touchPointInPage];
}

/*!
 @brief Method updates snapshot and dragged cell positions and visibilities on finish dragging
 */
- (void)finishDraggingCell
{
    [UIView animateWithDuration:[_config animationDuration] animations:^
    {
        [_draggedCellSnapshot setCenter:[_superView convertPoint:[_draggedCell center] fromView:_collectionView]];
        _draggedCellSnapshot.transform = CGAffineTransformMakeScale([_config scaleZoomNo], [_config scaleZoomNo]);
        _draggedCell.alpha = [_config alphaRevealedCell];
    }
    completion:^(BOOL finished)
    {
        [_draggedCellSnapshot setHidden:YES];
        [_draggedCell setHidden:NO];
    }];
}

#pragma mark - private

/*!
 @brief Method updates snapshot visibility while dragging
 */
- (void)updateSnapshotStateByGesturePoint:(CGPoint)touchPointInPage
{
    CGPoint touchPointInCollection = [_collectionView convertPoint:touchPointInPage fromView:_superView];
    BOOL isInsideCollectionView = CGRectContainsPoint([_collectionView bounds], touchPointInCollection);

    if (isInsideCollectionView)
    {
        _draggedCellSnapshot.alpha = [_config alphaSnapshotCorrectLocation];
    }
    else
    {
        _draggedCellSnapshot.alpha = [_config alphaSnapshotIncorrectLocation];
    }
}

/*!
 @brief Method summs two CGPoint-s
 */
CGPoint SumCGPoints(CGPoint p1, CGPoint p2)
{
    return (CGPoint){p1.x + p2.x, p1.y + p2.y };
}

@end

//
//  MMMDragCellInterfaceController.h
//  
//
//  Created by Kholomeev on 11/26/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MMMDragCellInterfaceController : NSObject

@property(nonatomic, weak) id delegate;


/*!
 @brief Method inits MMMDragCellInterfaceController with collection view
 */
- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

/*!
 @brief Method does animation on start dragging
 */
- (void)initiateDraggingByIndexPath:(NSIndexPath *)indexPath touchPointInCollection:(CGPoint)touchPointInCollection;

/*!
 @brief Method updates snapshot position and visibility while dragging
 */
- (void)updateViewsByTouchPointInPage:(CGPoint)touchPointInPage;

/*!
 @brief Method updates snapshot and dragged cell positions and visibilities on finish dragging
 */
- (void)finishDraggingCell;

@end

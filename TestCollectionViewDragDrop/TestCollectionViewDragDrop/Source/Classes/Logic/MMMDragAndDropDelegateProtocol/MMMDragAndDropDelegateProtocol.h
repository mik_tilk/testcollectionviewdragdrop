//
//  MMMDragAndDropDelegateProtocol.h
//  
//
//  Created by Kholomeev on 11/27/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMMDragAndDropDelegateProtocol <NSObject>

- (void)dragController:(id)dragController moveItemAtIndexPath:(NSIndexPath *)indexFrom toIndexPath:(NSIndexPath *)indexTo;

@optional
- (void)dragController:(id)dragController willStartDraggingIndexPath:(NSIndexPath *)indexPath;
- (void)dragController:(id)dragController willDeleteCellByIndexPath:(NSIndexPath *)indexPath;
- (void)dragControllerWillFinishDragging:(id)dragController;

@end

//
//  MMMDragDropConstants.h
//  TestCollectionViewDragDrop
//
//  Created by Kholomeev on 12/5/15.
//  Copyright © 2015 Kholomeev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MMMDragDropConstants : NSObject

@property(nonatomic, assign) NSInteger scrollSpeedSlow;
@property(nonatomic, assign) NSInteger scrollSpeedFast;
@property(nonatomic, assign) NSInteger scrollSlowArea;
@property(nonatomic, assign) NSInteger scrollFastArea;

@property(nonatomic, assign) CGFloat animationDuration;
@property(nonatomic, assign) CGFloat scaleZoomIn;
@property(nonatomic, assign) CGFloat scaleZoomNo;
@property(nonatomic, assign) CGFloat alphaSnapshotCorrectLocation;
@property(nonatomic, assign) CGFloat alphaSnapshotIncorrectLocation;
@property(nonatomic, assign) CGFloat alphaHiddenCell;
@property(nonatomic, assign) CGFloat alphaRevealedCell;

@end
